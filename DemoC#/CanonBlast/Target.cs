﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanonBlast
{
    class Target
    {
        private int m_x;
        private int m_y;

        public Target (int x)
        {

            m_x = x;
            m_y = 500;
        }

        public int GetX()
        {
            return m_x;
        }


        public int GetY()
        {
            return m_y;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanonBlast
{
    class Engine
    {

        private int m_target_num;
        private int m_target_min_x;
        private int m_target_max_x;

        private int m_gun_capacity;
        private int m_gun_x;
        private int m_gun_y;

        private int m_bullet_num;
        private int m_score;

        private bool m_gameover;

        public List<Bullet> bullets = new List<Bullet>();
        public List<Target> targets = new List<Target>();
        public Gun gun;

        public Engine ()
        {
            m_target_num = 3;
            m_target_max_x = 700;
            m_target_min_x = 200;

            m_gun_capacity = 130;
            m_gun_x = 10;
            m_gun_y = 500;

            gun = new Gun(m_gun_x, m_gun_y, m_gun_capacity);

            bullets.Add(new Bullet(m_gun_x, m_gun_y));

            Random rnd = new Random();
            for (int i = 0; i < m_target_num; i++)
            {
                int x = rnd.Next(m_target_min_x, m_target_max_x);
                targets.Add(new Target(x));
            }

            m_bullet_num = 20;
            m_gameover = false;
        }



        public bool IsGameOver()
        {
            return m_gameover;
        }


        public void AimUp ()
        {
            gun.Down();
        }



        public void AimDown()
        {
            gun.Up();
        }

        public int BulletLeft ()
        {
            return m_bullet_num;
        }

        public int Score()
        {
            return m_score;
        }


        public void Shot()
        {
            if (m_bullet_num > 0)
            {
                bullets[bullets.Count - 1].Shot(gun.GetAimX(), gun.GetAimY(), gun.GetCapacity());
                bullets.Add(new Bullet(m_gun_x, m_gun_y));

                m_bullet_num--;
            }
            else
            {
                m_gameover = true;
            }
        }


        public void Update()
        {
            foreach (var bul in bullets)
                bul.Bullet_Upd();
        }


        public void Collise(int index)
        {
            Random rnd = new Random();
            if (bullets[index].IsDown() == true)
            {

                for (var j = targets.Count - 1; j >= 0; j--)
                {
                    if (((bullets[index].GetX() - targets[j].GetX()) < 60) && ((bullets[index].GetX() - targets[j].GetX()) > 0))
                    {
                        m_score++;
                        targets.RemoveAt(j);
                        targets.Add(new Target(rnd.Next(m_target_min_x, m_target_max_x)));
                    }
                }

                bullets.RemoveAt(index);

            }
        }

    }
}

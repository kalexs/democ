﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CanonBlast
{
    public partial class FormGame : Form
    {

        private BufferedGraphicsContext context;
        private BufferedGraphics grafx;

        private Engine engine = new Engine();

        private System.Windows.Forms.Timer timer1;

        private void InitializeTimer()
        {
            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 20;
            timer1.Tick += new EventHandler(this.OnTimer);
            timer1.Enabled = true;
        }
        public FormGame() : base()
        {

            InitializeComponent();
            this.KeyDown += new KeyEventHandler(this.KeyDownHandler);
            this.Resize += new EventHandler(this.OnResize);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);

            InitializeTimer();

            context = BufferedGraphicsManager.Current;
            context.MaximumBuffer = new Size(this.Width + 1, this.Height + 1);
            grafx = context.Allocate(this.CreateGraphics(), new Rectangle(0, 0, this.Width, this.Height));
            
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            DrawToBuffer(grafx.Graphics);



        }




        private void KeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                engine.AimUp();
            }

            if (e.KeyCode == Keys.Up)
            {
                engine.AimDown();
            }

            if (e.KeyCode == Keys.Space)
            {
                engine.Shot();
            }
        }


        private void OnTimer(object sender, EventArgs e)
        {
            engine.Update();
            DrawToBuffer(grafx.Graphics);
            this.Refresh();
        }

        private void OnResize(object sender, EventArgs e)
        {
            context.MaximumBuffer = new Size(this.Width + 1, this.Height + 1);
            if (null != grafx)
            {
                grafx.Dispose();
                grafx = null;
            }
            grafx = context.Allocate(this.CreateGraphics(), new Rectangle(0, 0, this.Width, this.Height));
            DrawToBuffer(grafx.Graphics);
            this.Refresh();
        }

        private void DrawToBuffer(Graphics g)
        {
            g.FillRectangle(Brushes.White, 0, 0, this.Width, this.Height);
            grafx.Graphics.FillRectangle(Brushes.White, 0, 0, this.Width, this.Height);

            SolidBrush s = new SolidBrush(Color.Blue); 
            Pen black_pen = new Pen(Color.Black, 5);
            Pen red_pen = new Pen(Color.Red, 5);
            Pen green_pen = new Pen(Color.Green, 10);

            String drawString = "Bullets: " + engine.BulletLeft().ToString();
            Font drawFont = new Font("Arial", 16);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            Font gameoverFont = new Font("Arial", 35);
            SolidBrush gameoverBrush = new SolidBrush(Color.Red);



            g.DrawLine(black_pen, 0, 510, 2000, 510);
            g.DrawString(drawString, drawFont, drawBrush, 10, 10);

            
            drawString = "Score: " + engine.Score().ToString();

            g.DrawString(drawString, drawFont, drawBrush, 500, 10);

            if (engine.IsGameOver())
            {
                drawString = "GAME OVER";
                g.DrawString(drawString, gameoverFont, gameoverBrush, 10, 50);

                drawString = "Your result is " + engine.Score().ToString(); ;
                g.DrawString(drawString, drawFont, drawBrush, 10, 150);
            }

            Point point1 = new Point(engine.gun.GetX(), engine.gun.GetY());
            Point point2 = new Point(engine.gun.GetX() + (int)(engine.gun.GetAimX() * 50), engine.gun.GetY() - (int)(engine.gun.GetAimY() * 50));
            Point[] curvePoints =
             {
                 point1,
                 point2,
             };


            for (var index = engine.bullets.Count - 1; index >= 0; index--)
            {
                g.DrawEllipse(black_pen, new Rectangle(engine.bullets[index].GetX(), engine.bullets[index].GetY(), 5, 5));
                if (!engine.bullets[index].IsShoten())
                { 
                    g.DrawPolygon (green_pen, curvePoints);
                    g.DrawEllipse(black_pen, new Rectangle(engine.gun.GetX() + (int)(engine.gun.GetAimX() * 70), engine.gun.GetY() - (int)(engine.gun.GetAimY() * 70), 1, 1)); 
                }
                engine.Collise(index);
            }

            for (var index = engine.targets.Count - 1; index >= 0; index--)
            {
                g.DrawRectangle(red_pen, engine.targets[index].GetX(), engine.targets[index].GetY(), 50, 1);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            grafx.Render(e.Graphics);
        }
    }
}

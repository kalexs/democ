﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanonBlast
{
    class Bullet
    {

        private int m_x;
        private int m_y;

        private int m_home_x;
        private int m_home_y;

        private bool m_isdown;

        private bool m_isshot;

        private double m_delta;
        private double m_gravity;

        private double m_speed_x;
        private double m_speed_y;
        public Bullet(int x, int y)
        {
            m_x = x;
            m_y = y;

            m_home_x = x;
            m_home_y = y;

            m_delta = 0.15;
            m_gravity = 20;

            m_isshot = false;
            m_isdown = false;
        }



        public void Bullet_Upd ()
        {
            if (m_isshot)
            {
                m_x += (int)(m_speed_x * m_delta);
                m_y -= (int)(m_speed_y * m_delta);

                m_speed_y -= (uint)(m_gravity * m_delta);
            }

            if (m_y > 500)
            {
                Reset();
            }
        }




        public void Shot (double aim_x, double aim_y, double speed)
        {
            if (!m_isshot)
            {
                m_speed_x = speed * aim_x;
                m_speed_y = speed * aim_y;

                m_isshot = true;

            }
        }



        public int GetX()
        {
            return m_x;
        }


        public int GetY()
        {
            return m_y;
        }


        public bool IsShoten ()
        {
            return m_isshot;
        }




        public bool IsDown ()
        {
            return m_isdown;
        }


        private void Reset ()
        {
            m_isdown = true;

            m_speed_x = 0;
            m_speed_y = 0;
        }
    }
}

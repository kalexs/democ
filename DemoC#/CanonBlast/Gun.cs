﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanonBlast
{
    class Gun
    {
        private int m_x;
        private int m_y;

        private double m_theta;

        private double m_capacity;

        private double m_aim_x;
        private double m_aim_y;


        private void GetAim()
        {
            m_aim_x = Math.Cos(m_theta);
            m_aim_y = Math.Sin(m_theta);
        }



        public double GetAimX()
        {
            return m_aim_x;
        }


        public double GetAimY()
        {
            return m_aim_y;
        }



        public int GetX()
        {
            return m_x;
        }


        public int GetY()
        {
            return m_y;
        }

        public double GetCapacity()
        {
            return m_capacity;
        }


        public Gun (int x, int y, double capacity)
        {

            m_theta = Math.PI/4;

            GetAim();

            m_x = x;
            m_y = y;

            m_capacity = capacity;
        }

        public void Up ()
        {
            m_theta += 0.02;
            if (m_theta > Math.PI/2)
                m_theta = Math.PI / 2;
            GetAim();
        }



        public void Down()
        {
            m_theta -= 0.02;
            if (m_theta < 0)
                m_theta = 0;
            GetAim();
        }

    }
}

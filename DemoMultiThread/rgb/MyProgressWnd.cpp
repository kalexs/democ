// MyProgressWnd.cpp: ���� ����������
//

#include "stdafx.h"
#include "rgb.h"
#include "MyProgressWnd.h"


// MyProgressWnd

IMPLEMENT_DYNAMIC(MyProgressWnd, CWnd)

MyProgressWnd::~MyProgressWnd()
{
}


BEGIN_MESSAGE_MAP(MyProgressWnd, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void MyProgressWnd::create(void)
{
	CRect rc;
	GetClientRect(&rc);

	int x = rc.Width();
	int y = rc.Height();
	int n = 200;

	m_Img.create(x, y);
}

void MyProgressWnd::update(int pos1, int pos2, int max)
{
	m_Img.fill(pos1, pos2, max);
}

void MyProgressWnd::OnPaint()
{
	CPaintDC dc(this);
	m_Img.draw(&dc);
}
// ����������� ��������� MyProgressWnd



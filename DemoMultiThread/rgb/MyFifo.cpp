#include "stdafx.h"
#include "MyFifo.h"

void MyFifo::create(int x, int y, int &n, int frame_num)
	{
		::InitializeCriticalSection(&m_CS);
		m_buffer = new CImage_NSU[frame_num];
		m_frame_num = frame_num;
		m_ind_write.clear();
		m_read = 0;
		m_write = frame_num/2;
		m_good = true;
		for (int i = 0; i < m_frame_num; i++)
		{
			m_buffer[i].create(x, y);
			m_buffer[i].fill(n);
			m_ind_read.insert(m_ind_read.begin(), i);
		}
	}

void MyFifo::read(CPaintDC *pDC)
{
	::EnterCriticalSection(&m_CS);
	if (m_good)
	{
		m_buffer[m_read].draw(pDC);
		if (m_read == (m_frame_num - 1))
			m_read = 0;
		else
			m_read++;
		if ((m_read == m_write)||((m_read+1)==m_write))
			m_good = false;
	}
	::LeaveCriticalSection(&m_CS);
}

void MyFifo::write(int &count)
{
	::EnterCriticalSection(&m_CS);
		m_buffer[m_write].fill(count);
		if (m_write != (m_frame_num - 1))
			m_write++;
		else
			m_write = 0;
		m_good = true;
	::LeaveCriticalSection(&m_CS);
}

int MyFifo::GetSize()
{
	return m_ind_read.size();
}

int MyFifo::Stats()
{
	if (m_write > m_read)
		return (m_read + (m_frame_num - m_write));
	else
		return (m_read - m_write);
}



#pragma once

class CImage_NSU
{
public:
	CImage_NSU() 
	{
		m_pData = NULL;
	};

	CImage_NSU(BYTE *p_data, int w, int h)
	{
		m_pData = (COLORREF*)p_data;
		//::InitializeCriticalSection(&m_CS);
		ZeroMemory(&m_bmp, sizeof(m_bmp));
		m_bmp.biSize = sizeof(m_bmp);
		m_bmp.biPlanes = 1;
		m_bmp.biWidth = w;
		m_bmp.biHeight = h;
		m_bmp.biBitCount = 32;

	};

	~CImage_NSU() 
	{ 
		if (NULL != m_pData)
		{
			delete[] m_pData;
			m_pData = NULL;
		}
	};

	void fill(int &count);
	void draw(CPaintDC *pDC);
	void create(int w, int h);
	CRITICAL_SECTION m_CS;
private:
	COLORREF *m_pData;
	BITMAPINFOHEADER m_bmp;
};


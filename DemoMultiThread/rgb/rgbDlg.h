
// rgbDlg.h : ���� ���������
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "MyPaintWnd.h"
#include "MyProgressWnd.h"



// ���������� ���� CrgbDlg
class CrgbDlg : public CDialogEx
{
// ��������
public:
	CrgbDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_RGB_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CSliderCtrl m_SlideR;
	CEdit m_EditR;
	CSpinButtonCtrl m_SpinR;
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnEnChangeEdit1();
	CEdit m_InformBox;
	CSliderCtrl m_SlideG;
	CEdit m_EditG;
	CSpinButtonCtrl m_SpinG;
	afx_msg void OnEnChangeEdit3();
	CSliderCtrl m_SlideB;
	CEdit m_EditB;
	CSpinButtonCtrl m_SpinB;
	afx_msg void OnEnChangeEdit4();
	MyPaintWnd m_Paint;
	void draw_pict(CString str);
	afx_msg void OnTimer(UINT id);
	CProgressCtrl m_Progress_Scale;
	CEdit m_RewriteText;
	CEdit m_RereadText;
	afx_msg void OnStnClickedPlace();
	MyProgressWnd m_Progress;
};

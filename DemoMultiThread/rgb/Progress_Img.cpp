#include "stdafx.h"
#include "Progress_Img.h"

void Progress_Img::create(int w, int h)
{
	m_pData = new COLORREF[w*h];
	ZeroMemory(&m_bmp, sizeof(m_bmp));
	m_bmp.biSize = sizeof(m_bmp);
	m_bmp.biPlanes = 1;
	m_bmp.biWidth = w;
	m_bmp.biHeight = h;
	m_bmp.biBitCount = 32;
}

void Progress_Img::fill(float pos1, float pos2, float max)
{

	//int size = m_bmp.biHeight * m_bmp.biWidth;
	for (int y = 0; y<m_bmp.biHeight; y++)
	{
		int pos11 = (int)(pos1 / max*m_bmp.biWidth);
		int pos22 = (int)(pos2 / max*m_bmp.biWidth);
		for (int x = 0; x < m_bmp.biWidth; x++)
		{
			if (x == pos11)
				m_pData[y * m_bmp.biWidth + x] = RGB(0, 0, 0);
			if (x == pos22)
				m_pData[y * m_bmp.biWidth + x] = RGB(0, 0, 255);
			if (pos22 > pos11)
			{
				if ((x > pos22) || (x<pos11))
					m_pData[y * m_bmp.biWidth + x] = RGB(0, 255, 0);
				else
					m_pData[y * m_bmp.biWidth + x] = RGB(255, 0, 0);
			}
			if (pos22 < pos11)
			{
				if ((x > pos22) && (x<pos11))
					m_pData[y * m_bmp.biWidth + x] = RGB(0, 255, 0);
				else
					m_pData[y * m_bmp.biWidth + x] = RGB(255, 0, 0);
			}
		}
	}
	//for (int y = 0; y<m_bmp.biHeight; y++) 
	//{
	//	size -= m_bmp.biWidth;
	//	m_pData[size + count] = RGB(0, 0, 0);
	//	m_pData[size + count - 1] = RGB(255, 255, 255);
	//	//::Sleep(10);
	//}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//HANDLE nThread = (HANDLE)_beginThreadex(void *security = NULL, unsigned size = 0, unsigned(*address_t) (void*), void *pDate, unsigned Initflag = 0, unsigned *threadID);
//
//void __stdcall thread(void *pData)
//{
//	CMyClass *p = (CMyClass*)pData;
//	p->work();
//}
//
//class CMyClass
//{
//public:
//	void CreateThread();
//private:
//	HANDLE m_nThread;
//	HANDLE m_nEventExit;
//};
//
//void CMyClass::CreateThread()
//{
//	unsigned nThreadID;
//	m_nThread = (HANDLE)_beginThreadex(NULL, 0, &thread, this, 0, &nThreadID);
//}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Progress_Img::draw(CPaintDC *pDC)
{
	::SetDIBitsToDevice(*pDC, 0, 0, m_bmp.biWidth, m_bmp.biHeight, 0, 0, 0, m_bmp.biHeight, m_pData, (BITMAPINFO*)&m_bmp, 0);
}

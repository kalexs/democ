#pragma once

#include "IWrite.h"
#include "IRead.h"
#include "Image_NSU.h"
#include <vector>

using namespace std;

class MyFifo :
	public IWrite, IRead
{
public:

	void create(int x, int y, int &n, int frame_num);
	void write(int &count);
	void read(CPaintDC *pDC);

	int m_frame_num;
	int m_rewrite;
	int m_reread;

	int m_write;
	int m_read;

	bool m_good;

	int GetSize();
	int Stats();
	CRITICAL_SECTION m_CS;
private:

	CImage_NSU *m_buffer;
	vector<int> m_ind_read;
	vector<int> m_ind_write;
};


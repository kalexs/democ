#pragma once

#include "MyFifo.h"



class CWriter
{

public:

	HANDLE m_hExitThread;
	HANDLE m_hThread;
	DWORD m_dwTimeOut = 100;
	IWrite *m_pFifo;
	int m_count;

	void start(IWrite *fifo, int &count);
	void stop();
	void work();

};



